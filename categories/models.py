from django.db import models


class Category(models.Model):
    name = models.CharField('name', max_length=255)
    path = models.CharField('path', max_length=255, null=True, blank=True, db_index=True)

    def get_parent(self):
        if not self.path:
            return Category.objects.none()
        return Category.objects.get()

    def add_child(self, child):
        if not self.path:
            path = self.pk
        else:
            path = '{}.{}'.format(self.path, self.pk)
        Category.objects.filter(pk=child.pk).update(path=path)

    def delete(self, *args, **kwargs):
        self.get_children().delete()
        super().delete(*args, **kwargs)

    def get_parents(self):
        if not self.path:
            return Category.objects.none()
        return Category.objects.filter(pk__in=self.path.split('.')).order_by('-path')

    def get_children(self):
        return Category.objects.filter(path__startswith='{}.'.format(self.path))

    def get_siblings(self):
        return Category.objects.filter(path=self.path)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['path']
