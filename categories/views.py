from .models import Category
from .serializers import CategorySerializer, CategoryDetailedSerializer
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import viewsets


class CategoryViewSet(viewsets.ModelViewSet):
    queryset = Category.objects.all()
    serializer_class = CategoryDetailedSerializer
