from rest_framework import serializers
from .models import Category


class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = ['id', 'name']


class CategoryDetailedSerializer(CategorySerializer):
    parents = serializers.SerializerMethodField()
    children = serializers.SerializerMethodField()
    siblings = serializers.SerializerMethodField()

    def get_parents(self, obj):
        return CategorySerializer(obj.get_parents(), many=True).data

    def get_children(self, obj):
        return CategorySerializer(obj.get_children(), many=True).data

    def get_siblings(self, obj):
        return CategorySerializer(obj.get_siblings(), many=True).data

    class Meta:
        model = Category
        fields = ['id', 'name', 'parents', 'children', 'siblings']